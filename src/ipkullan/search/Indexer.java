package ipkullan.search;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.util.Date;
import java.text.*;


public class Indexer {

    private static int counter_g;

    public static void main(String[] args) throws Exception {

	System.out.println("Usage: java " + Indexer.class.getName() + " <index dir> <data dir1> <data dir1> ..");
	
	File indexDir = new File(args[0]); // Index Dir

	String [] dataDirs = new String[args.length-1]; 

	for(int i = 0; i < args.length-1; i++)
	    {
		dataDirs[i] = args[i+1];
	    }

	// File dataDir = new File(dataDirs[0]);  // Data Dir

	counter_g = 1;
	long start = new Date().getTime();

	//	int numIndexed = index(indexDir, dataDir);
	int numIndexed=0;
	numIndexed=index(indexDir, dataDirs);
	long end = new Date().getTime();
	System.out.println("Indexing " + numIndexed + " files took " + (end - start) + " ms");
  }

    public static int index(File indexDir, String[] dataDirs)
    
	throws IOException {
	int counter = 0;
	System.out.println("DATADIRS GROESSE: " + Integer.toString(dataDirs.length));	
	IndexWriter writer = new IndexWriter(indexDir, new StandardAnalyzer(), true);
	writer.setUseCompoundFile(false);
	int numIndexed = 0;
	File dataDir;
	for(int i = 0; i < dataDirs.length; i++)
	    {
		dataDir = new File(dataDirs[i]);
		if (!dataDir.exists() || !dataDir.isDirectory()) 
		    throw new IOException(dataDir + " don't exist or is not a directory");
		indexDirectory(writer, dataDir, counter);
		numIndexed += writer.docCount();
		System.out.println(Integer.toString(i) + " directory indexing ended");
	    }
	
	writer.optimize();
	writer.close();
	return numIndexed;
    }
    
    private static void indexDirectory(IndexWriter writer, File dir, int counter)
	throws IOException {

	int counter_D = 0;
	File[] files = dir.listFiles();

	for (int i = 0; i < files.length; i++) {
	    File f = files[i];

	    if (f.isDirectory()) {
		indexDirectory(writer, f, counter_D);  // recurse
	    }

	    else if (f.getName().endsWith(".pdf") || f.getName().endsWith(".epub") || f.getName().endsWith(".djvu")) {
		indexFile(writer, f, counter_D);
	    } 
	    // else { System.out.println(f.getCanonicalPath());}
	}
    }
    
    public static String humanReadableByteCount(long bytes, boolean si) {
	int unit = si ? 1000 : 1024;
	if (bytes < unit) return bytes + " B";
	int exp = (int) (Math.log(bytes) / Math.log(unit));
	String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
	return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private static void indexFile(IndexWriter writer, File f, int counter)
	throws IOException {

	if (f.isHidden() || !f.exists() || !f.canRead()) {
	    return;
	}

	System.out.println("Indexing " + counter_g + " " + f.getCanonicalPath());
	counter_g++;

	Document doc = new Document();
	doc.add(Field.Text("contents", new FileReader(f)));
	doc.add(Field.Text("filepath", f.getCanonicalPath()));
	doc.add(Field.Text("filename", f.getName()));

	DateFormat df = new SimpleDateFormat("MM/dd/yyy HH:mm:ss");
	Date d = new Date(f.lastModified());
	String moddate = df.format(d);

	doc.add(Field.Text("date", moddate));
	doc.add(Field.Keyword("size", humanReadableByteCount(f.length(), false)));
	writer.addDocument(doc);
    }
}
