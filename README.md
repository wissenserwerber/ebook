# ebook

ebook is an application which indexes ebooks found in the given directories and exposes a web page for searching them.

For indexing and searching, Lucene library is used. Web page is written in Perl 5.

**Voraussetzungen**: JDK & lucene.jar sollen installiert worden sein.

### Compile

cd ~/java/ebook/src/ipkullan/search

javac -d ../../../bin/ -cp ../../../lib/lucene-1.4.3.jar Indexer.java Searcher.java

(Note: Codes are compiled with Java 8)

### Run Indexer

java -cp bin:lib/lucene-1.4.3.jar ipkullan.search.Indexer ~/lucene-index-dir-20170815 ~/Meine-Ebucher01 ~/Meine-Ebucher02 // Instead of Meine-Ebucher01 etc, enter your directories which contain your ebooks

### Running on Windows (using CMD.exe or a terminal like ConEmu)

java -cp "bin;lib/lucene-1.4.3.jar" ipkullan.search.Indexer ~/lucene-index-dir-20170815 ~/Meine-Ebucher01 ~/Meine-Ebucher02

(i.e. the double quotes around classpath are necessary for Windows and seperating the items with semicolon is required because Windows uses colon for designating drives e.g. C: \ )

### Run Searcher (at the command line)

java -cp bin:lib/lucene-1.4.3.jar ipkullan.search.Searcher ~/lucene-index-dir-20170815 Learning*

## ebook can be seen in action at [ipkullan.com/search](http://ipkullan.com/search)