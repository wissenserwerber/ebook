#!/usr/bin/perl -w

use strict;
use CGI qw(:all);
use warnings;

my $home = $ENV{'HOME'};

$ENV{'PATH'} = "$ENV{'PATH'}:"."$home/java";

my $basePath = "$home/java";
my $prjName = "ebook";
my $prjPath = $basePath . "/" . $prjName;

my $CLASSPATH = $prjPath . "/bin" . ":" . $prjPath . "/lib/lucene-1.4.3.jar";

my $packageName = "ipkullan.search";
my $className = "Searcher";
my $luceneIndexDir = "$home/lucene-index-dir-20171027";

# my $luceneIndexDir = ~/lucene-index-dir-20171027
my $this_url = self_url; 
my $query = "";

print header;
print start_html("Search");

print "<form action=\"" , $this_url , "\" method=\"get\">",
    "<label>Search ebook: </label>";
print <<HTML;   
    <input type="text" name="q" size=20 value="Visual Basic*"/>
    <input type ="submit"/>
    </form>
HTML

if (exists($ENV{QUERY_STRING})) { 
    
    $query = $ENV{'QUERY_STRING'}; 
    if ($query =~ m/q=/) { # Ob der Ausdruck 'q=' sich im Query befindet
	$query =~ s/^q=//;  # Falls ja, entfern es aus der query
	
	print "<h3>Results for $query</h3>";
	# 2016 24 05 Ursprüngliche Vorstellung	
	my $javaCmd = "java " . "-cp " . "$CLASSPATH " . "$packageName." . "$className " . "$luceneIndexDir " . "$query";
	
	system($javaCmd);
	print "<br /><a href='$this_url'>Return to search</a>";
    }
}
print end_html;
